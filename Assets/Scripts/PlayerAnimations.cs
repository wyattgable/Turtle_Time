﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class PlayerAnimations : MonoBehaviour {


    public int counter = 1;
    public Animator number_Anim;

	public void Upgrade()
    {
		if (counter >= 4) {
			
			gameObject.GetComponent<Text>().text = "Get Ready!";
			gameObject.GetComponent<Outline> ().enabled = false;
		} else {
			counter += 1;
			gameObject.GetComponent<Text>().text = "" + counter;
		}
        
            
    }

    public void TurnOff()
    {
        number_Anim.enabled = false;
    }

}
