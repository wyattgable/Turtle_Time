﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class TutleSelection : MonoBehaviour {

    public GameObject[] turtles;
    public int selection;
    Vector3 offScreenLocation;
    public GameObject eventSystem;
    public Text playerNumber;
    public Text[] arcadePlayerNumber;
    public int counter = 1;
    public GameObject[] mode;
    public Color cpuColor;
    public Animator player_Anim;
	public AudioSource sfxSource;
    public AudioClip [] selectionSfx;
   

public void TurtlePick()
    {
        if (turtles[selection].transform.position.y < 26)
        {
            turtles[selection].transform.Translate(0f, .4f, 0f);
        }
        else if (counter == 5)
        {
            //Game Start
            eventSystem.SetActive(true);
            mode[0].SetActive(false);
            mode[1].SetActive(true);
        }
        else
        {
            
            eventSystem.SetActive(true);
            //playerNumber.text = "" + counter;
            
        }
        
    }
    void Update()
    {
        TurtlePick();

    }
    void Awake()
    {
        selection = 5;
    }
public void QuickStart()
    {
        for (int i = 0; i < 4; i++)
        {
            if (arcadePlayerNumber[i].text == "")
            {
               // arcadePlayerNumber[i].text = "" + counter;
                arcadePlayerNumber[i].color = cpuColor;
               // counter++;
            }
        }
        
    }
public void TurtleWalkAway()
    {

    }

    //What Turtle Gets Effected.
    public void Select()
    {
        sfxSource.PlayOneShot(selectionSfx[0]);
        player_Anim.enabled = true;
        arcadePlayerNumber[0].text = "" + counter;
        selection = 0;
        counter++;
    }
    public void Select2()
    {
        sfxSource.PlayOneShot(selectionSfx[1]);
        player_Anim.enabled = true;
        arcadePlayerNumber[1].text = "" + counter;
        selection = 1;
        counter++;
    }
    public void Select3()
    {
        sfxSource.PlayOneShot(selectionSfx[2]);
        player_Anim.enabled = true;
        arcadePlayerNumber[2].text = "" + counter;
        selection = 2;
        counter++;
    }
    public void Select4()
    {
        sfxSource.PlayOneShot(selectionSfx[3]);
        player_Anim.enabled = true;
        arcadePlayerNumber[3].text = "" + counter;
        selection = 3;
        counter++;
    }

}
