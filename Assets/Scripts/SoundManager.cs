﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class SoundManager : MonoBehaviour {

    
    public GameObject endGame;
    public AudioSource sfx;
    public AudioClip[] sounds;
    public int counter;

    //On Finish Line
    void OnCollisionEnter2D()
    {
        
        sfx.Stop();
        sfx.PlayOneShot(sounds[counter]);
        counter++;
        if (counter == 4)
        {
            StartCoroutine(wait());
        }
    }
    IEnumerator wait()
    {
        yield return new WaitForSeconds(1.5f);
        endGame.SetActive(true);    
    }

}


