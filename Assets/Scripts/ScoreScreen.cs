﻿using UnityEngine;
using System.Collections;

public class ScoreScreen : MonoBehaviour {

    public AudioSource sfxSource;
    public AudioClip sfxClip;
    public AudioClip iceCreamSfxClip;
    public void PlaySound()
    {
        sfxSource.PlayOneShot(sfxClip);
    }
    public void PlayIcecreamSound()
    {
        sfxSource.PlayOneShot(iceCreamSfxClip);
    }
}
