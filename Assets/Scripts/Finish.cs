﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class Finish : MonoBehaviour
{
    public Sprite[] trophys;
    public string nameo;
    public GameObject holder;
    public GameObject endgameText;
    public static int counter;
    public Color cpuColor;
    void OnCollisionEnter2D()
    {
        holder.SetActive(true);
      
       holder.GetComponent<Image>().sprite = trophys[counter];
        counter++;
        
            if (counter == 1)
        {
            if (gameObject.GetComponentInChildren<Text>().color == cpuColor)
            {
               
                endgameText.GetComponent<Text>().text = "" + "Winner: Turtle " + nameo;
            }

            else
            {
                endgameText.GetComponent<Text>().text = "" + "Winner: Player " + gameObject.GetComponentInChildren<Text>().text;
            }
           
           
        }
       
        //gameObject.GetComponentInChildren<Text>().text = "" + counter;
        gameObject.GetComponent<Rigidbody2D>().gravityScale = 0;
        gameObject.GetComponent<Animator>().enabled = false;

    }

    }
