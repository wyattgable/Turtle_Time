﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class CountDown : MonoBehaviour {

    public Text countdownText;
    public int counter;
    public AudioSource arcadeTheme;
    public AudioSource sfx;
    public AudioClip [] countdownSfx;

    //set counter to 3

        void Awake()
    {
        sfx.PlayOneShot(countdownSfx[3]);
    }
    public void CountDownCounter()
    {
        counter--;
        switch (counter)
        {
            case -1:
                gameObject.SetActive(false);
                break;
            case 0:
                sfx.PlayOneShot(countdownSfx[0]);

               countdownText.text = "" + "Go!";
                arcadeTheme.enabled = true;
                break;
            case 1:
                sfx.PlayOneShot(countdownSfx[1]);

               countdownText.text = "" + counter;
                break;
            case 2:
                countdownText.text = "" + counter;
                sfx.PlayOneShot(countdownSfx[2]);
                break;
        }

    }

}
