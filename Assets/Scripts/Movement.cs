﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Movement : MonoBehaviour
{

    public AudioClip[] arcade_Sounds;
    public AudioSource sfxSource;
    private Animator animator;
    private int roll;

    public void Awake()
    {
        animator = GetComponent<Animator>();
        StartCoroutine(wait());

    }
    IEnumerator wait()
    {
        yield return new WaitForSeconds(2.5f);
        animator.enabled = true;
    }
    public bool Fall
    {
        get { return animator.GetBool("Fall"); }
        set { animator.SetBool("Fall", value); }

    }

    public void BasicMovement()
    {
        int luck = Random.Range(0, 10);
        CheckLuck(luck);


    }
    void CheckLuck(int outcome)
    {
        if (outcome == 2)
        {
            gameObject.GetComponent<Rigidbody2D>().isKinematic = true;
            Fall = true;
            sfxSource.PlayOneShot(arcade_Sounds[0]);
            //activate fall anim
        }
        else if (gameObject.GetComponent<Rigidbody2D>().gravityScale <= -.08 && outcome == 3 || outcome == 4 || outcome == 5)
            {
                gameObject.GetComponent<Rigidbody2D>().isKinematic = true;
                Fall = true;
            sfxSource.PlayOneShot(arcade_Sounds[0]);
                //activate fall anim          
        }
        else
        {
            switch (Random.Range(0, 10))
            {
                case 0:
                    gameObject.GetComponent<Rigidbody2D>().gravityScale = -.01f;
                    break;
                case 1:
                    gameObject.GetComponent<Rigidbody2D>().gravityScale = -.02f;
                    break;
                case 2:
                    gameObject.GetComponent<Rigidbody2D>().gravityScale = -.03f;
                    break;
                case 3:
                    gameObject.GetComponent<Rigidbody2D>().gravityScale = -.04f;
                    break;
                case 4:
                    gameObject.GetComponent<Rigidbody2D>().gravityScale = -.05f;
                    break;
                case 5:
                    gameObject.GetComponent<Rigidbody2D>().gravityScale = -.06f;
                    break;
                case 6:
                    gameObject.GetComponent<Rigidbody2D>().gravityScale = -.07f;
                    break;
                case 7:
                    gameObject.GetComponent<Rigidbody2D>().gravityScale = -.08f;
                    break;
                case 8:
                    gameObject.GetComponent<Rigidbody2D>().gravityScale = -.09f;
                    break;
                case 9:
                    gameObject.GetComponent<Rigidbody2D>().gravityScale = -.1f;
                    break;
                case 10:
                    gameObject.GetComponent<Rigidbody2D>().gravityScale = -.01f;
                    break;
            }

        }
    }

    void CheckIfGetUp()
    {
        if (Random.Range(0, 2) == 1)
        {
            Fall = false;
            gameObject.GetComponent<Rigidbody2D>().isKinematic = false;
            switch (Random.Range(0, 2))
            {
                case 0:
                    gameObject.GetComponent<Rigidbody2D>().gravityScale = -.08f;
                    break;
                case 1:
                    gameObject.GetComponent<Rigidbody2D>().gravityScale = -.12f;
                    break;
                case 2:
                    gameObject.GetComponent<Rigidbody2D>().gravityScale = -.05f;
                    break;

            }
        }
    }
}


